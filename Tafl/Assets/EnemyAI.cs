﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TACTIC {RANDOM,ONEMOVE}

public class EnemyAI : MonoBehaviour
{
    public TACTIC tactic;
    public STATE team;
    public player mainPlayer;



    public class move
    {
        public pawn peice;
        public boardTile toTile;
        public move(pawn peice, boardTile toTile)
        {
            this.peice = peice;
            this.toTile = toTile;
        }
    }

    private void Start()
    {

    }

    public List<move> getAllMoves()
    {
        List<move> moves;
        moves = new List<move>();
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Pawn"))
        {
            pawn p = g.GetComponent<pawn>();
            if (p.team == STATE.ATTACK)
            {
                foreach (boardTile b in mainPlayer.getPossibleMoves(p.currentTile, false))
                {
                    moves.Add(new move(p, b));
                }
            }
        }
        return moves;
    }

    public void takeMove()
    {
        switch (tactic)
        {
            case TACTIC.RANDOM:
                {
                    List<move> possibleMoves = getAllMoves();
                    move chosenMove = possibleMoves[Random.Range(0, possibleMoves.Count)];
                    mainPlayer.makeMove(chosenMove.peice, chosenMove.toTile);
                    break;
                }
            case TACTIC.ONEMOVE:
                {
                    Dictionary<IntVector2, STATE> boardState = mainPlayer.representBoardState();
                    List<move> possibleMoves = getAllMoves();
                    int maxVal = int.MinValue;
                    move bestMove = null;
                    foreach (move m in possibleMoves)
                    {
                        int moveValue = checkValue(m.toTile, boardState);
                        if (moveValue > maxVal)
                        {
                            bestMove = m;
                            maxVal = moveValue;
                        }
                    }
                    mainPlayer.makeMove(bestMove.peice,bestMove.toTile);
                    break;
                }
        }
    }

    public int checkValue(boardTile moveTile, Dictionary<IntVector2, STATE> boardState)
    {
        IntVector2 startLocation = new IntVector2(moveTile.row, moveTile.column);
        int resultingValue = 0;
        //Rows + 
        //if the tile next to the square is an enemy
        IntVector2 checkTile = new IntVector2(startLocation.x + 1, startLocation.y);
        if (boardState.ContainsKey(checkTile) && IsEnemy(boardState[checkTile], team))
        {
            //and the tile after that is friendly
            checkTile = new IntVector2(startLocation.x + 2, startLocation.y);
            if (boardState.ContainsKey(checkTile) && IsFriendly(boardState[checkTile], team))
            {
                //capture the peice;
                resultingValue += 1;
            }
        }
        //Rows -
        checkTile = new IntVector2(startLocation.x - 1, startLocation.y);
        if (boardState.ContainsKey(checkTile) && IsEnemy(boardState[checkTile], team))
        {
            checkTile = new IntVector2(startLocation.x - 2, startLocation.y);
            if (boardState.ContainsKey(checkTile) && IsFriendly(boardState[new IntVector2(startLocation.x - 2, startLocation.y)], team))
            {
                resultingValue += 1;
            }
        }
        //Columns +
        checkTile = new IntVector2(startLocation.x, startLocation.y+1);
        if (boardState.ContainsKey(checkTile) && IsEnemy(boardState[new IntVector2(startLocation.x, startLocation.y + 1)], team))
        {
            checkTile = new IntVector2(startLocation.x, startLocation.y + 2);
            if (boardState.ContainsKey(checkTile) && IsFriendly(boardState[new IntVector2(startLocation.x, startLocation.y + 2)], team))
            {
                resultingValue += 1;
            }
        }
        //Columns -
        checkTile = new IntVector2(startLocation.x, startLocation.y-1);
        if (boardState.ContainsKey(checkTile) && IsEnemy(boardState[new IntVector2(startLocation.x, startLocation.y - 1)], team))
        {
            checkTile = new IntVector2(startLocation.x, startLocation.y - 2);
            if (boardState.ContainsKey(checkTile) && IsFriendly(boardState[new IntVector2(startLocation.x, startLocation.y - 2)], team))
            {
                resultingValue += 1;
            }
        }

        return resultingValue;
    }

    public bool IsEnemy(STATE checkTile, STATE AITeam)
    {
        if (checkTile == AITeam || checkTile == STATE.RESTRICTED  || checkTile == STATE.EMPTY)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool IsFriendly(STATE checkTile, STATE AITeam)
    {
        if (checkTile == AITeam || checkTile == STATE.RESTRICTED)
        {

            return true;
        }
        else
        {
            return false;
        }
    }


}
