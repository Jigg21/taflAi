﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pawn : MonoBehaviour
{
    public int row;
    public int column;
    public STATE team;
    public boardTile currentTile;
    private MeshRenderer mesh;
    private Color originalColor;
    // Start is called before the first frame update
    void Start()
    {
        row = currentTile.row;
        column = currentTile.column;
        currentTile.setPeice(this);
        mesh = GetComponent<MeshRenderer>();
        originalColor = mesh.material.color;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void select()
    {
        mesh.material.color = Color.green;
    }

    public void deselect()
    {
        mesh.material.color = originalColor;
    }
}
