﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class IntVector2
{
    public int x;
    public int y;

    public IntVector2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public class EqualityComparer : IEqualityComparer<IntVector2>
    {

        public bool Equals(IntVector2 x, IntVector2 y)
        {
            return x.x == y.x && x.y == y.y;
        }

        public int GetHashCode(IntVector2 x)
        {
            return x.x ^ x.y;
        }

    }

}

public class player : MonoBehaviour
{

    public bool againstAI;
    public EnemyAI enemy;





    public List<boardTile> possibleMoves;
    private pawn currentSelected;
    private STATE turn;
    private Dictionary<IntVector2,boardTile> boardState;
    // Start is called before the first frame update
    void Start()
    {
        boardState = new Dictionary<IntVector2, boardTile>(new IntVector2.EqualityComparer());
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Board"))
        {
            boardTile b = g.GetComponent<boardTile>();
            boardState.Add(new IntVector2(b.row, b.column), b);
        }
        turn = STATE.DEFEND;
    }

    // Update is called once per frame
    void Update()
    {
        if (againstAI && turn != STATE.DEFEND)
        {
            enemy.takeMove();
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(cameraRay,out hit))
            {
                if (hit.collider.tag == "Pawn")
                {
                    pawn hitpawn = hit.collider.GetComponent<pawn>();
                    if (turn == hitpawn.team)
                    {
                        if (currentSelected != null)
                        {
                            currentSelected.deselect();
                        }
                        currentSelected = hitpawn;
                        currentSelected.select();
                        List<boardTile> newMoves = getPossibleMoves(currentSelected.currentTile, false);
                        showPossibleMoves(possibleMoves, newMoves);
                        possibleMoves = newMoves;
                    }

                }

                if (hit.collider.tag == "King")
                {
                    pawn king = hit.collider.GetComponent<pawn>();
                    if (turn == STATE.DEFEND)
                    {
                        if (currentSelected != null)
                        {
                            currentSelected.deselect();
                        }
                        currentSelected = king;
                        currentSelected.select();
                        List<boardTile> newMoves = getPossibleMoves(currentSelected.currentTile, true);
                        showPossibleMoves(possibleMoves, newMoves);
                        possibleMoves = newMoves;
                    }
                }

                if (hit.collider.tag == "Board" && currentSelected != null)
                {
                    
                    boardTile selectedBoard = hit.collider.GetComponent<boardTile>();
                    foreach (boardTile b in possibleMoves)
                    {
                        if (b == selectedBoard)
                        {
                            makeMove(currentSelected, selectedBoard);
                        }
                    }
                }

            }
        }
    }

    //Make a move and update the board state
    public void makeMove(pawn peice, boardTile toTile)
    {
        Vector3 newlocation = new Vector3(toTile.transform.position.x, .4f, toTile.transform.position.z);
        peice.transform.position = newlocation;
        peice.currentTile.state = STATE.EMPTY;
        peice.currentTile.setPeice(null);
        peice.currentTile = toTile;
        toTile.state = peice.team;
        peice.row = toTile.row;
        peice.column = toTile.column;
        peice.deselect();
        resetPossibleMoves();
        toTile.setPeice(peice);
        checkForCapture(toTile,turn);
        if (turn == STATE.DEFEND)
        {
            turn = STATE.ATTACK;
        }
        else
        {
            turn = STATE.DEFEND;
        }
    }

    //Check if the last move causes a capture
    public void checkForCapture(boardTile startTile, STATE team)
    {
        IntVector2 startLocation = new IntVector2(startTile.row, startTile.column);
        //Rows + if the tile next to the square is an enemy
        if (IsEnemy(new IntVector2(startLocation.x +1, startLocation.y), team))
        {
            //and the tile after that is friendly
            if (isFriendly(new IntVector2(startLocation.x +2, startLocation.y),team))
            {
                //capture the peice;
                boardTile capturedTile = boardState[new IntVector2(startLocation.x + 1, startLocation.y)];
                capturedTile.state = STATE.EMPTY;
                Destroy(capturedTile.peice);
            }
        }
        //Rows -
        if (IsEnemy(new IntVector2(startLocation.x - 1, startLocation.y), team))
        {
            if (isFriendly(new IntVector2(startLocation.x - 2, startLocation.y), team))
            {
                boardTile capturedTile = boardState[new IntVector2(startLocation.x - 1, startLocation.y)];
                capturedTile.state = STATE.EMPTY;
                Destroy(capturedTile.peice);
            }
        }
        //Columns +
        if (IsEnemy(new IntVector2(startLocation.x, startLocation.y + 1), team))
        {
            if (isFriendly(new IntVector2(startLocation.x, startLocation.y + 2), team))
            {
                boardTile capturedTile = boardState[new IntVector2(startLocation.x, startLocation.y + 1)];
                capturedTile.state = STATE.EMPTY;
                Destroy(capturedTile.peice);
            }
        }
        //Columns -
        if (IsEnemy(new IntVector2(startLocation.x, startLocation.y - 1), team))
        {
            if (isFriendly(new IntVector2(startLocation.x, startLocation.y - 2), team))
            {
                boardTile capturedTile = boardState[new IntVector2(startLocation.x, startLocation.y - 1)];
                capturedTile.state = STATE.EMPTY;
                Destroy(capturedTile.peice);
            }
        }

    }

    //Check if the tile contains an enemy
    public bool IsEnemy (IntVector2 checkTile, STATE team)
    {
        if (boardState.ContainsKey(checkTile))
        {
            return boardState[checkTile].state != STATE.EMPTY && boardState[checkTile].state != team;
        }else
        {
            return false;
        }

    }

    //Check if the tile contains a friendly unit
    public bool isFriendly (IntVector2 checkTile, STATE team)
    {
        if (boardState.ContainsKey(checkTile))
        {
            return boardState[checkTile].state == STATE.RESTRICTED || boardState[checkTile].state == team;
        }
        else
        {
            return false;
        }

    }

    //Get all possible moves for a peice
    public List<boardTile> getPossibleMoves(boardTile startTile, bool isKing)
    {
        IntVector2 originalPosition = new IntVector2(startTile.row, startTile.column);
        List<boardTile> moves = new List<boardTile>();
        if (!isKing)
        {
            for (int i = originalPosition.x + 1; i < 12; i++)
            {
                boardTile checkTile = boardState[new IntVector2(i, originalPosition.y)];
                if (checkTile.state != STATE.EMPTY)
                {
                    break;
                }
                else
                {
                    moves.Add(checkTile);
                }
            }
            for (int i = originalPosition.x - 1; i > 0; i--)
            {
                boardTile checkTile = boardState[new IntVector2(i, originalPosition.y)];
                if (checkTile.state != STATE.EMPTY)
                {
                    break;
                }
                else
                {
                    moves.Add(checkTile);
                }
            }
            for (int i = originalPosition.y + 1; i < 12; i++)
            {
                boardTile checkTile = boardState[new IntVector2(originalPosition.x, i)];
                if (checkTile.state != STATE.EMPTY)
                {
                    break;
                }
                else
                {
                    moves.Add(checkTile);
                }
            }
            for (int i = originalPosition.y - 1; i > 0; i--)
            {
                boardTile checkTile = boardState[new IntVector2(originalPosition.x, i)];
                if (checkTile.state != STATE.EMPTY)
                {
                    break;
                }
                else
                {
                    moves.Add(checkTile);
                }
            }
        }else
        {
            for (int x = -1; x < 2; ++x)
            {
                boardTile checkTile = boardState[new IntVector2(originalPosition.x + x, originalPosition.y)];
                if (checkTile.state == STATE.EMPTY)
                {
                    moves.Add(checkTile);
                }
            }
            for (int y = -1; y < 2; ++y)
            {
                boardTile checkTile = boardState[new IntVector2(originalPosition.x, originalPosition.y + y)];
                if (checkTile.state == STATE.EMPTY)
                {
                    moves.Add(checkTile);
                }
            }
        }

        return moves;
    }

    //Show the player all possible moves
    private void showPossibleMoves(List<boardTile> oldMoves, List<boardTile> newMoves)
    {
        foreach(boardTile b in oldMoves)
        {
            b.resetMove();
        }

        foreach (boardTile b in newMoves)
        {
            b.displayMove();
        }
    }

    //Hide all possible moves
    private void resetPossibleMoves()
    {
        foreach (boardTile b in possibleMoves)
        {
            b.resetMove();
        }
    }

    //Represent the board state as a dictionary for enemy ai
    public Dictionary<IntVector2,STATE> representBoardState()
    {
        Dictionary<IntVector2, STATE> result = new Dictionary<IntVector2, STATE>(new IntVector2.EqualityComparer());
        foreach (boardTile b in boardState.Values)
        {
            result.Add(new IntVector2(b.row, b.column), b.state);
        }
        return result;
    }

}
