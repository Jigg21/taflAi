﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum STATE { EMPTY, ATTACK, DEFEND, RESTRICTED }


public class boardTile : MonoBehaviour
{
    public int row;
    public int column;
    public STATE state;
    public GameObject peice;

    private MeshRenderer mesh;
    private Color OriginalColor;
    
    private void Start()
    {
        mesh = GetComponent<MeshRenderer>();
    }

    public void displayMove()
    {
        OriginalColor = mesh.material.color;
        mesh.material.color = Color.green;
    }

    public void resetMove()
    {
        mesh.material.color = OriginalColor;
    }

    public void setPeice(pawn newPeice)
    {
        if (newPeice!= null)
        {
            peice = newPeice.gameObject;
        }else
        {
            peice = null;
        }

    }
}
