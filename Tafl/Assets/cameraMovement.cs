﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMovement : MonoBehaviour
{
    public float speed;
    public float angle;
    public GameObject rotateobj;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horiz = Input.GetAxis("Horizontal");
        if ( horiz != 0)
        {
            rotateobj.transform.eulerAngles += new Vector3(0,speed * horiz,0);
        }
    }
}
